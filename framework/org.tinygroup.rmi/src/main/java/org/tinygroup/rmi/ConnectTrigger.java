package org.tinygroup.rmi;

public interface ConnectTrigger {
	String REREG = "ReReg";
	void deal();
	String getType();
}
