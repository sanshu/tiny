package org.tinygroup.cepcore;


public interface CEPCoreOperator {
	void startCEPCore(CEPCore cep) ;
	void stopCEPCore(CEPCore cep) ;
	void setCEPCore(CEPCore cep);
}
